
import numpy as np
import pickle
import Element as element
import cv2
import os


class Imatge(element.Element):
    def __init__(self):
        super().__init__()
        self._img=None
        self._representacio=None
    @property
    def img(self):
        return self._img
    @property
    def representacio(self):
        return self._representacio
    def set_representacio(self,array_img):
        self._representacio=array_img
    
    def get_representacio_bow(self,vocabulari_imatge):
        def compute_bow_images(img, bow_extractor):
            sift = cv2.SIFT_create()
            keypoints = sift.detect(img)
            if keypoints != []:
                bow = bow_extractor.compute(img, keypoints)
            else:
                bow = np.zeros((1, bow_extractor.descriptorSize()))
            return bow

        img_grey=cv2.cvtColor(self._img,cv2.COLOR_BGR2GRAY)
        self._representacio=compute_bow_images(img_grey, vocabulari_imatge)
        
    def get_representacio_tf(self,vocabulari_imatge):
        def compute_bow_images(img, bow_extractor):
            sift = cv2.SIFT_create()
            keypoints = sift.detect(img)
            if keypoints != []:
                bow = bow_extractor.compute(img, keypoints)
            else:
                bow = np.zeros((1, bow_extractor.descriptorSize()))
            return bow
        
        img_grey=cv2.cvtColor(self._img,cv2.COLOR_BGR2GRAY)
        self._representacio=compute_bow_images(img_grey, vocabulari_imatge)
        
        fitxer=open("idf.txt",'r')
        idf= np.array([float(linia) for linia in fitxer])
        self._representacio=(self._representacio/len(self._img))*idf
        
        
    def llegeix(self, nom_fitxer):
        self._img = cv2.imread(nom_fitxer)
        c=nom_fitxer.find("/")
        c=nom_fitxer[c+1:] 
        self._nom_fitxer=c
        self._etiqueta="img"
    
    def calcula_distancia(self, img):
        numerador=0
        denominador=0

        numerador = np.sum(np.minimum(self._representacio, img._representacio))
        denominador= min(self._representacio.sum(), img._representacio.sum())
        distance = numerador / denominador
        return 1 - distance
    
    def calcula_distancia_cos(self,img):
        numerador=0
        denominador=0
        numerador = np.sum(self._representacio, img._representacio)
        denominador= np.sqrt(np.sum(self._representacio^2))*np.sqrt(np.sum(img._representacio^2))
        distance = numerador / denominador
        return 1 - distance




