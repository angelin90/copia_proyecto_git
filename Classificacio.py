import numpy as np
import cv2
import pickle
import re
import os
import Missatge
import Imatge
import index
import random
import numpy as np
import Model as mod
class Classificacio:
    def __init__(self):
        self._vocabulari_missatge=None
        self._vocabulari_imatge=None
        self._cjt_entrenament=[]
        self._obj_index=None
        self._agrupa=None
    def llegeix_vocabulari(self,tipus_doc,tipus_repr):
        
        if tipus_doc=="txt":
            lvocabulari=[]
            if tipus_repr=="bow":
                fitxer=open("vocabulary.txt",'r')
                for linia in fitxer:
                    linia=linia[:-1]
                    lvocabulari.append(linia)
                self._vocabulari_missatge=lvocabulari
            elif tipus_repr=="tf-idf":
                fitxer=open("vocabulary_idf.txt",'r')
                for linia in fitxer:
                    linia=linia[:-1]
                    linia=linia.split(" ")
                    tupla_linia=(linia[0],float(linia[1]))
                    lvocabulari.append(tupla_linia)
                self._vocabulari_missatge=lvocabulari
            
        elif tipus_doc=="img":
            
            def init_bow_images(file_vocabulary):
                sift = cv2.SIFT_create()
                matcher = cv2.FlannBasedMatcher() 
                with open(file_vocabulary, 'rb') as fitxer:
                    vocabulary = pickle.load(fitxer)
                bow_extractor = cv2.BOWImgDescriptorExtractor(sift, matcher)
                bow_extractor.setVocabulary(vocabulary)
                return bow_extractor
            
            bow_extractor=init_bow_images("vocabulary.dat")
            self._vocabulari_imatge=bow_extractor
            
    def llegeix_entrenament(self,directori,tipus_doc,tipus_repr,tipus_m):
        
        if tipus_doc=="txt":
            
            I=index.Index()
            I.crea_diccionari(self._vocabulari_missatge,tipus_doc,tipus_repr)
            
            llista_fitxers = os.listdir(directori)
            if tipus_repr=="bow":
                for i in llista_fitxers:
                    a=Missatge.Missatge()
                    if tipus_m=="recuperacio":
                        a.llegeix('newsgroups/train/'+i)
                    elif tipus_m=="agrupament":
                        a.llegeix('clustering_text/'+i)
                    a.get_representacio_bow(self._vocabulari_missatge)
                    self._cjt_entrenament.append(a)
                    
                    llista=a.text
                    posicio=llista_fitxers.index(i)
                    I.afegeix_identificador(llista,posicio,tipus_doc)
            elif tipus_repr=="tf-idf":
                 for i in llista_fitxers:
                    a=Missatge.Missatge()
                    if tipus_m=="recuperacio":
                        a.llegeix('newsgroups/train/'+i)
                    elif tipus_m=="agrupament":
                        a.llegeix('clustering_text/'+i)
                    a.get_representacio_tf(self._vocabulari_missatge)
                    self._cjt_entrenament.append(a)
                    
                    posicio=llista_fitxers.index(i)
                    print(posicio)
                    llista=a.text
                    I.afegeix_identificador(llista,posicio,tipus_doc)
            self._obj_index=I
        elif tipus_doc=="img":

            llista_imatges = os.listdir(directori)
            if tipus_repr=="bow":
                for i in llista_imatges:
                    a=Imatge.Imatge()
                    if tipus_m=="recuperacio":
                        a.llegeix('cifar/train/'+i)
                    elif tipus_m=="agrupament":
                        a.llegeix('clustering/'+i)
                    a.get_representacio_bow(self._vocabulari_imatge)
                    self._cjt_entrenament.append(a)
                    
                    llista=a.img
                    posicio=llista_imatges.index(i)

            elif tipus_repr=="tf-idf":
                for i in llista_imatges:
                    a=Imatge.Imatge()
                    if tipus_m=="recuperacio":
                        a.llegeix('cifar/train/'+i)
                    elif tipus_m=="agrupament":
                        a.llegeix('clustering/'+i)
                    a.get_representacio_tf(self._vocabulari_imatge)
                    self._cjt_entrenament.append(a)
                    
                    llista=a.img

            
    def agrupa(self,tipus_doc,tipus_dis,k):
        llista_grups=[]
        llista_grups=[[i,None,[]] for i in range(k)]
        llistat_entrenament=self._cjt_entrenament.copy()
        referent=random.choice(llistat_entrenament)
        distancia=[]
        min_dis=1
        a=0
        
        #AGAFA REPRESENTANT RANDOM
        for i in range(k):
            rnd=random.choice(llistat_entrenament)
            llista_grups[i][1]=rnd
            llista_grups[i][2].append(rnd)
            llistat_entrenament.remove(rnd)
            
        #PER AQUEST REPRESENTANT ASSIGNA LES IMATGES A CADA GRUP
        for i in llistat_entrenament:
            for representant in range(k):
                if tipus_dis=="normal":
                    dis=llista_grups[representant][1].calcula_distancia(i)
                elif tipus_dis=="cos":
                    dis=llista_grups[representant][1].calcula_distancia_cos(i)
                distancia.append((dis,representant))
            
            for j in range(len(distancia)):
                if distancia[j][0]<min_dis:
                    min_dis=distancia[j][0]
            for j in range(len(distancia)):
                if min_dis==distancia[j][0]:
                    grup=distancia[j][1]
                    llista_grups[grup][2].append(i)
            distancia=[]
            min_dis=1
            
            #repetim el proces 10 cops
            
        if tipus_doc=="txt":
            while a<10:
                for i in range(k):
                    suma=0
                    new_dict = {}
                    llista=llista_grups[i][2].copy()
                    # dic_base=llista[0].representacio
                    for key,value in referent.representacio.items():
                        for num in range(len(llista)):
                            d=llista[num].representacio
                            suma+=d[key]
                        if len(llista)!=0:
                            new_dict.setdefault(key, suma/len(llista))    
                        suma=0
                    llista_grups[i][2]=[]
                    M=Missatge.Missatge()
                    M.set_representacio(new_dict)
                    llista_grups[i][1]=M
                #Redistrivuimm els fitxer pels grups
                for i in llistat_entrenament:
                    for representant in range(k):
                        if tipus_dis=="normal":
                            dis=llista_grups[representant][1].calcula_distancia(i)
                        elif tipus_dis=="cos":
                            dis=llista_grups[representant][1].calcula_distancia_cos(i)
                        distancia.append((dis,representant))
                    
                    for j in range(len(distancia)):
                        if distancia[j][0]<min_dis:
                            min_dis=distancia[j][0]
                    for j in range(len(distancia)):
                        if min_dis==distancia[j][0]:
                            grup=distancia[j][1]
                            llista_grups[grup][2].append(i)
                    distancia=[]
                    min_dis=1
                print("Agrupacio n",a)
                a+=1
            self._agrupa=llista_grups
            # print(self._agrupa)
            
        elif tipus_doc=="img":

            while a<10:
                for i in range(k):
                    creazeros=referent.representacio
                    shap=creazeros.shape
                    
                    suma=np.zeros(shap)
                    llista=llista_grups[i][2].copy()
                    
                    for b in range(len(llista)):
                        suma+=llista[b].representacio
                    nou_repr=suma/len(llista)
                    
                    llista_grups[i][2]=[]
                    I=Imatge.Imatge()
                    I.set_representacio(nou_repr)
                    llista_grups[i][1]=I
                    
                    
                for i in llistat_entrenament:
                    for representant in range(k):
                        if tipus_dis=="normal":
                            dis=llista_grups[representant][1].calcula_distancia(i)
                        elif tipus_dis=="cos":
                            dis=llista_grups[representant][1].calcula_distancia_cos(i)
                        distancia.append((dis,representant))
                    
                    for j in range(len(distancia)):
                        if distancia[j][0]<min_dis:
                            min_dis=distancia[j][0]
                    for j in range(len(distancia)):
                        if min_dis==distancia[j][0]:
                            grup=distancia[j][1]
                            llista_grups[grup][2].append(i)
                    distancia=[]
                    min_dis=1
                print("Agrupacio n",a)
                a+=1
            self._agrupa=llista_grups
            # print(self._agrupa)
    
    
    def guarda_model(self,doc,repre,dis,m):
        if m=="recuperacio":
            if doc=="txt":
                M=mod.Model(self._cjt_entrenament,self._vocabulari_missatge,self._obj_index)
            elif doc=="img":
                M=mod.Model(self._cjt_entrenament,None,self._obj_index)
        elif m=="agrupament":
            if doc=="txt":
                M=mod.Model(self._cjt_entrenament,self._vocabulari_missatge,self._agrupa)
            elif doc=="img":
                M=mod.Model(self._cjt_entrenament,None,self._agrupa)
        fitxer=open("Model/"+doc+"_"+repre+"_"+dis+"_"+m+".pkl","wb")
        pickle.dump(M,fitxer)
        fitxer.close()
        
        
        
    def classifica(self,nom_fitxer,model,tipus,dis,tipus_m,tipus_repr):
        
        
        if tipus=="txt":
            dic=dict()
            if tipus_m=="recupera":
                with open("Model/"+model, 'rb') as fitxer:
                    mCopia = pickle.load(fitxer)
                cjt=mCopia.get_cjt()
                voc=mCopia.get_voc()
                I=mCopia.get_index_o_agrupa()
                
                
                Missatge_a_classificar=Missatge.Missatge()
                Missatge_a_classificar.llegeix('newsgroups/test/'+nom_fitxer)
                if tipus_repr=="bow":
                    Missatge_a_classificar.get_representacio_bow(voc)
                else:
                    Missatge_a_classificar.get_representacio_tf(voc)
                    
                llista_doc_dis=I.recupera_llista(Missatge_a_classificar,"txt")
                for pos in llista_doc_dis:
                    if dis=="normal":
                        print()
                        dic[cjt[pos]]=Missatge_a_classificar.calcula_distancia(cjt[pos])
                    else:
                        dic[cjt[pos]]=Missatge_a_classificar.calcula_distancia_cos(cjt[pos])
                dic={k: v for k, v in sorted(dic.items(), key=lambda item: item[1])}
                lista=dic.keys()
                llista_final=list(lista)
                return llista_final
        
        elif tipus=="img":
            if tipus_m=="recupera":
                
                with open("Model/"+model, 'rb') as fitxer:
                        mCopia = pickle.load(fitxer)
                cjt=mCopia.get_cjt()
                    
            Imatge_a_classificar=Imatge.Imatge()
            Imatge_a_classificar.llegeix('cifar/test/'+nom_fitxer)
            if tipus_repr=="bow":
                Imatge_a_classificar.get_representacio_bow(self._vocabulari_imatge)
            else:
                Imatge_a_classificar.get_representacio_tf(self._vocabulari_imatge)
            dic=dict()
            for i in cjt:
                if dis=="normal":
                    dic[i]=Imatge_a_classificar.calcula_distancia(i)
                else:
                    dic[i]=Imatge_a_classificar.calcula_distancia_cos(i)
            dic={k: v for k, v in sorted(dic.items(), key=lambda item: item[1])}
            lista=dic.keys()
            llista_final=list(lista)
            return llista_final
        
    def classifica_agrupa(self,n,model,tipus,dis):
        
        dic=dict()
        llista_final=[]
        if n==1:
            with open("Model/"+model, 'rb') as fitxer:
                    mCopia = pickle.load(fitxer)
            agrupa=mCopia.get_index_o_agrupa()
            
            for i in range(len(agrupa)):
                for mis in agrupa[i][2]:
                    if dis=="normal":
                        dic[mis]=agrupa[i][1].calcula_distancia(mis)
                    else:
                        dic[mis]=agrupa[i][1].calcula_distancia_cos(mis)
                dic={k: v for k, v in sorted(dic.items(), key=lambda item: item[1])}
                lista=dic.keys()
                llista_grup=list(lista)
                llista_final.append(llista_grup)
            return llista_final,len(agrupa)
        if n==2:
            def pedirNumeroEntero():
 
                correcto=False
                num=0
                while(not correcto):
                    try:
                        num = int(input("Introdueix un numero enter: "))
                        correcto=True
                    except ValueError:
                        print('Error, introdueix un numero enter')
                 
                return num
            
            with open("Model/"+model, 'rb') as fitxer:
                    mCopia = pickle.load(fitxer)
            agrupa=mCopia.get_index_o_agrupa()
            
            # print(agrupa)
            
            print("")
            grup=-1
            while grup>len(agrupa) or grup<0:
                print("Hi han ",len(agrupa)," grups: Quin vols visualitzar?")
                grup= pedirNumeroEntero()-1
                
            if len(agrupa[grup][2])==0:
                if grup==len(agrupa)-1:
                    grup-=1
                else:
                    grup+=1
            for mis in agrupa[grup][2]:
                if dis=="normal":
                    dic[mis]=agrupa[grup][1].calcula_distancia(mis)
                else:
                    dic[mis]=agrupa[grup][1].calcula_distancia_cos(mis)
            dic={k: v for k, v in sorted(dic.items(), key=lambda item: item[1])}
            lista=dic.keys()
            llista_grup=list(lista)
            return llista_grup
            
                
                
            
        