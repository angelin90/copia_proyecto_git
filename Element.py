from abc import ABC,abstractmethod

class Element(ABC):
    def __init__(self):
        self._nom_fitxer=None
        self._etiqueta=None
    @abstractmethod
    def llegeix(self,nom_fitxer):
        raise NotImplementedError()