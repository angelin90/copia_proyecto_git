import re
import numpy as np
import Element as element
import math
class Missatge(element.Element):
    def __init__(self):
        super().__init__()
        self._text=None
        self._representacio=None
        self._text_read=None
    
    @property
    def text(self):
        return self._text
    def set_representacio(self,dic):
        self._representacio=dic
    @property
    def text_read(self):
        return self._text_read
    @property
    def representacio(self):
        return self._representacio
    
    def llegeix(self,nom_fitxer):
        
        llistat_text=[]
        fitxer=open(nom_fitxer,'r')
        
        for linia in fitxer:
            lin=linia[:-1]
            low=lin.lower()
            paraules = re.sub("[^a-zA-Z0-9]", " ", low).split()
            llistat_text.extend(paraules)
        
        c=nom_fitxer.find("/")
        c=nom_fitxer[c+1:]
        self._nom_fitxer=c
        self._text=llistat_text
        self._etiqueta="txt"
        fitxer.close()
        fitxer=open(nom_fitxer,'r')
        self._text_read=fitxer.read()
    def get_representacio_bow(self,vocabulari):
        self._representacio = {valor: 0 for valor in vocabulari}
        for i in self._text:
            if i in self._representacio:
                self._representacio[i] += 1
    def get_representacio_tf(self,vocabulari):
        total_paraules=len(self._text)
        voc_claus=[]
        for paraula in vocabulari:
            voc_claus.append(paraula[0])
            
        representacio_provisional = {valor: 0 for valor in voc_claus}
        self._representacio = {valor: 0 for valor in voc_claus}
        
        for i in self._text:
            if i in representacio_provisional:
                representacio_provisional[i] += 1
        
        for i in self._text:
            if i in representacio_provisional:
                for idf in vocabulari:
                    if i==idf[0]:
                        self._representacio[i]=(representacio_provisional[i]/total_paraules)*idf[1]
    def calcula_distancia(self,m):
        numerador=0
        denominador1=0
        denominador2=0
        for i in self._representacio:
            numerador=numerador+min(self._representacio[i],m._representacio[i])
            denominador1 +=self._representacio[i]
            denominador2 +=m._representacio[i]
        denominador=min(denominador1,denominador2)
        if denominador!=0:
            distancia=1-numerador/denominador
        else:
            distancia=1
        return distancia
    def calcula_distancia_cos(self,m):
        numerador=0
        denominador=0
        d1=0
        d2=0
        for i in self._representacio:
            numerador=numerador+(self._representacio[i]*m._representacio[i])
            d1 += self._representacio[i]**2
            d2 += m._representacio[i]**2
        denominador=math.sqrt(d1)*math.sqrt(d2)
        if denominador!=0:
            distancia=1-numerador/denominador
        else:
            distancia=1
        return distancia
    
        
        
        
        
        
        
        
        
        
        
        
        