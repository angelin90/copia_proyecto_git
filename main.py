import Classificacio as clss
import os
# Importem el mòdul de visualització
import matplotlib.pyplot as plt
# Importem el mòdul per llegir i tractar imatges
import matplotlib.image as mpimg

var = 8
rar=9
print("ana"+ var)
`print("juan"+rar)


def visualitza(a,tipus,k):
    if k==1:
        fig, axs = plt.subplots()
        if tipus=="txt":
            final=a.text_read
            axs.axis('off')
            axs.text(0, 1, final, va = 'top', clip_on = True, fontsize = 'xx-small')
        else:
            img_final=a.img
            axs.axis('off')
            axs.imshow(img_final)
            
    else:
        fig, axs = plt.subplots(1,k)
        if tipus=="txt":
            for i,text in enumerate(a):
                final=text.text_read
                # print(final)
                axs[i].axis('off')
                axs[i].text(0, 1, final, va = 'top', clip_on = True, fontsize = 'xx-small')
        else:
            for i,img in enumerate(a):
                img_final=img.img
                axs[i].axis('off')
                axs[i].imshow(img_final)
def pedirNumeroEntero():
 
    correcto=False
    num=0
    while(not correcto):
        try:
            num = int(input("Introdueix un numero enter: "))
            correcto=True
        except ValueError:
            print('Error, introdueix un numero enter')
     
    return num

def crea_model(tipus_doc,tipus_repr,tipus_dis,tipus_m,train):
    if tipus_m=="recuperacio":
        C1=clss.Classificacio()
        C1.llegeix_vocabulari(tipus_doc,tipus_repr)
        C1.llegeix_entrenament(train,tipus_doc,tipus_repr,tipus_m)
        C1.guarda_model(tipus_doc,tipus_repr,tipus_dis,tipus_m)
        print("")
        print("   ******** Model Recuperacio Creat ********")
        print("")
    elif tipus_m=="agrupament":
        C1=clss.Classificacio()
        C1.llegeix_vocabulari(tipus_doc,tipus_repr)
        C1.llegeix_entrenament(train,tipus_doc,tipus_repr,tipus_m)
        # print("")
        # print("Numero de Grups:")
        # print("")
        # n_grups= pedirNumeroEntero()
        n_grups=3
        C1.agrupa(tipus_doc,tipus_dis,n_grups)
        C1.guarda_model(tipus_doc,tipus_repr,tipus_dis,tipus_m)
        print("")
        print("   ******** Model Agrupament Creat ********")
        print("")

def retrieval():
    salir = False
    opcion = 0
     
    while not salir:
     
        print ("1. Crea Model")
        print ("2. Visualitza Model")
        print ("0. Surt del programa")
         
        print ("Escull una opcio")
     
        opcion = pedirNumeroEntero()
     
        if opcion == 1:
            salir2 = False
            opcion2 = 0
             
            while not salir2:
                print("Escull un model a crear")
                print("")
                print ("1. Model txt,bow,interseccio,recuperacio")
                print ("2. Model txt,tf-idf,interseccio,recuperacio")
                print ("3. Model img,bow,interseccio,recuperacio")
                print ("4. Model img,tf-idf,interseccio,recuperacio")
                print ("5. Model txt,bow,interseccio,agrupacio")
                print ("6. Model txt,tf-idf,interseccio,agrupacio")
                print ("7. Model txt,bow,cosinus,agrupacio")
                print ("8. Model txt,tf-idf,cosinus,agrupacio")
                print ("9. Model img,bow,interseccio,agrupacio")
                # print ("10. Model img,tf-idf,interseccio,agrupacio")
                print ("10. Model img,tf-idf,cosinus,agrupacio")
                # print ("12. Model img,tf-idf,cosinus,agrupacio")
                print ("0. Torna al menu principal")
                 
                print ("Escull una opcio")
             
                opcion2 = pedirNumeroEntero()
             
                if opcion2 == 1:
                    crea_model("txt","bow","normal","recuperacio","newsgroups/train")
                elif opcion2 == 2:
                    print("Pot tardar 5 min- el numero te que arrivar a 2000")
                    crea_model("txt","tf-idf","normal","recuperacio","newsgroups/train")
                elif opcion2 == 3:
                    crea_model("img","bow","normal","recuperacio","cifar/train")
                elif opcion2 == 4:
                    crea_model("img","tf-idf","normal","recuperacio","cifar/train")
                elif opcion2 == 5:
                    crea_model("txt","bow","normal","agrupament","clustering_text")
                elif opcion2 == 6:
                    crea_model("txt","bow","cos","agrupament","clustering_text")
                elif opcion2 == 7:
                    crea_model("txt","tf-idf","normal","agrupament","clustering_text")
                elif opcion2 == 8:
                    crea_model("txt","tf-idf","cos","agrupament","clustering_text")
                elif opcion2 == 9:
                    crea_model("img","bow","normal","agrupament","clustering")
                # elif opcion2 == 10:
                #     crea_model("img","bow","cos","agrupament","clustering")
                elif opcion2 == 10:
                    crea_model("img","tf-idf","normal","agrupament","clustering")
                # elif opcion2 == 12:
                #     crea_model("img","tf-idf","cos","agrupament","clustering")
                elif opcion2 == 0:
                    salir2 = True
                else:
                    print ("Introdueix un numero per nevegar per el submenu Model")
             
            
        elif opcion == 2:
            salir3 = False
            opcion3 = 0
             
            while not salir3:
                print("Escull un model a crear")
                print("")
                print ("1. Visualitza Recupera")
                print ("2. Visualitza Agrupament")
                print ("0. Torna al submenu Model")
                
                print ("Escull una opcio")
             
                opcion3 = pedirNumeroEntero()
             
                if opcion3 == 1:
                    
                    print ("Models disponibles per visualitzar recupera")
                    # fitxer=open("Model/","r")
                    llista_fitxers = os.listdir("Model/")
                    for fitxer in llista_fitxers:
                        if "recuperacio" in fitxer:
                            print(fitxer)
                    eleccio=input("Escriu el nom del model que vols visualitzar:")
                    if eleccio in llista_fitxers:
                        query=input("Indica nom document o imatge query:")
                        if query in os.listdir("newsgroups/test") or query in os.listdir("cifar/test"):
                            C1=clss.Classificacio()
                            if "normal" in eleccio:
                                if "txt" in eleccio:
                                    tip="txt"
                                    if "bow" in eleccio:
                                        ll=C1.classifica(query,eleccio,"txt","normal","recupera","bow")
                                    else:
                                        ll=C1.classifica(query,eleccio,"txt","normal","recupera","tf-idf")
                                else:
                                    tip="img"
                                    if "bow" in eleccio:
                                        C1.llegeix_vocabulari("img","bow")
                                        ll=C1.classifica(query,eleccio,"img","normal","recupera","bow")
                                    else:
                                        C1.llegeix_vocabulari("img","tf-idf")
                                        ll=C1.classifica(query,eleccio,"img","normal","recupera","tf-idf")
                                    
                            else:
                                if "txt" in eleccio:
                                    tip="txt"
                                    if "bow" in eleccio:
                                        ll= C1.classifica(query,eleccio,"txt","cos","recupera","bow")
                                    else:
                                        ll= C1.classifica(query,eleccio,"txt","cos","recupera","tf-idf")
                                else:
                                    tip="img"
                                    if "bow" in eleccio:
                                        ll= C1.classifica(query,eleccio,"img","cos","recupera","bow")
                                    else:
                                        ll=C1.classifica(query,eleccio,"img","cos","recupera","tf-idf")
                            
                            # V=vv.Visualitzador()
                            pvi=0
                            pvf=5
                            print("")
                            print("!!!!!!!!!!! Informacio Important !!!!!!!!!!!")
                            print("Els textos o imatges es mostraran al sortir del programa")
                            print("En cas de text, cada columna del full, es un text")
                            print("")
                            visualitza(ll[pvi:pvf],tip,5)
                            
                            salir4 = False
                            opcion4 = 0
                             
                            while not salir4:
                                print("Nevegacio")
                                print("")
                                print ("1. Mostra 5 seguents")
                                print ("2. Mostra 5 anteriors")
                                print ("0. Torna Menu Anterior")
                                
                                print ("Escull una opcio")
                             
                                opcion4 = pedirNumeroEntero()
                             
                                if opcion4 == 1:
                                    if pvf+5<=len(ll):
                                        pvi+=5
                                        pvf+=5
                                        visualitza(ll[pvi:pvf],tip,5)
                                        print("Es mostrara al tancar el programa")
                                elif opcion4 == 2:
                                    if pvi-5>=0:
                                        pvi-=5
                                        pvf-=5
                                        visualitza(ll[pvi:pvf],tip,5)
                                        print("Es mostrara al tancar el programa")
                                elif opcion4 == 0:
                                    salir4 = True
                                else:
                                    print ("Introdueix un numero per nevegar per el submenu")
                        else:
                            print("")
                            print("ERROR - El fitxer no existeix")
                            print("")
                    else:
                        print("")
                        print("ERROR- El fitxer no existeix")
                        print("")
                elif opcion3 == 2:
                    
                    #Agrupament
                    salir5 = False
                    opcion5 = 0
                             
                    while not salir5:
                        print("Escull que vols visualitzar")
                        print("")
                        print ("1. Visualitza el mes similar de cada grup")
                        print ("2. Selecciona Grup")
                        print ("0. Torna Menu Anterior")
                        
                        print ("Escull una opcio")
                     
                        opcion5 = pedirNumeroEntero()
                        

                        if opcion5 == 1:
                            print ("Models disponibles per visualitzar recupera")
                            llista_fitxers = os.listdir("Model/")
                            for fitxer in llista_fitxers:
                                if "agrupament" in fitxer:
                                    print(fitxer)
                            eleccio=input("Escriu el nom del model que vols visualitzar:")
                            if eleccio in llista_fitxers:
                                C1=clss.Classificacio()
                                if "txt" in eleccio:
                                    if "normal" in eleccio:
                                        tip="txt"
                                        print(eleccio)
                                        ll,grups=C1.classifica_agrupa(1,eleccio,"txt","normal")
                                    else:
                                        tip="txt"
                                        print(eleccio)
                                        ll,grups=C1.classifica_agrupa(1,eleccio,"txt","cos")
                                else:
                                    if "normal" in eleccio:
                                        tip="img"
                                        ll,grups=C1.classifica_agrupa(1,eleccio,"img","normal")
                                    else:
                                        tip="img"
                                        ll,grups=C1.classifica_agrupa(1,eleccio,"img","cos")
                                
                                print("")
                                print("!!!!!!!!!!! Informacio Important !!!!!!!!!!!")
                                print("Els textos o imatges es mostraran al sortir del programa")
                                # print("En cas de text, cada columna del full, es un text")
                                print("")
                                for i in range(grups):
                                    visualitza(ll[i][0],tip,1)
                            else:
                                print("")
                                print("ERROR- El fitxer no existeix")
                                print("")

                        elif opcion5 == 2:
                            print ("Models disponibles per visualitzar recupera")
                            llista_fitxers = os.listdir("Model/")
                            for fitxer in llista_fitxers:
                                if "agrupament" in fitxer:
                                    print(fitxer)
                            eleccio=input("Escriu el nom del model que vols visualitzar:")
                            if eleccio in llista_fitxers:
                                C1=clss.Classificacio()
                                if "txt" in eleccio:
                                    if "normal" in eleccio:
                                        tip="txt"
                                        print(eleccio)
                                        ll=C1.classifica_agrupa(2,eleccio,"txt","normal")
                                    else:
                                        tip="txt"
                                        print(eleccio)
                                        ll=C1.classifica_agrupa(2,eleccio,"txt","cos")
                                else:
                                    if "normal" in eleccio:
                                        tip="img"
                                        ll=C1.classifica_agrupa(2,eleccio,"img","normal")
                                    else:
                                        tip="img"
                                        ll=C1.classifica_agrupa(2,eleccio,"img","cos")
                                pvi=0
                                pvf=5
                                print("")
                                print("!!!!!!!!!!! Informacio Important !!!!!!!!!!!")
                                print("Els textos o imatges es mostraran al sortir del programa")
                                print("En cas de text, cada columna del full, es un text")
                                print("")
                                visualitza(ll[pvi:pvf],tip,5)
                                
                                salir6 = False
                                opcion6 = 0
                                 
                                while not salir6:
                                    print("Nevegacio grup")
                                    print("")
                                    print ("1. Mostra 5 seguents")
                                    print ("2. Mostra 5 anteriors")
                                    print ("0. Torna Menu Anterior")
                                    
                                    print ("Escull una opcio")
                                 
                                    opcion6 = pedirNumeroEntero()
                                 
                                    if opcion6 == 1:
                                        if pvf+5<=len(ll):
                                            pvi+=5
                                            pvf+=5
                                            visualitza(ll[pvi:pvf],tip,5)
                                            print("Es mostrara al tancar el programa")
                                    elif opcion6 == 2:
                                        if pvi-5>=0:
                                            pvi-=5
                                            pvf-=5
                                            visualitza(ll[pvi:pvf],tip,5)
                                            print("Es mostrara al tancar el programa")
                                    elif opcion6 == 0:
                                        salir6 = True
                                    else:
                                        print ("Introdueix un numero per nevegar per el submenu")
                                
                                
                            else:
                                print("")
                                print("ERROR- El fitxer no existeix")
                                print("")

                            
                        elif opcion5 == 0:
                            salir5 = True
                elif opcion3 == 0:
                    salir3 = True
                else:
                    print ("Introdueix un numero per nevegar per el submenu Model")
            #MENU FINAL
        elif opcion == 0:
            salir = True
        else:
            print ("Introdueix un numero per nevegar per el menu")
     
    print ("FINS AVIAT!")
    
    

#Executar sense paramatres    
retrieval()





