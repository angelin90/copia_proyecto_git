class Index:
    def __init__(self):
        self._dict_index={}
    
    def crea_diccionari(self,vocabulari,tipus_doc,tipus_repr):
        
        if tipus_doc=="txt":
            if tipus_repr=="bow":
                self._dict_index = {valor: [] for valor in vocabulari}
            elif tipus_repr=="tf-idf":
                voc_claus=[]
                for paraula in vocabulari:
                    voc_claus.append(paraula[0])
                self._dict_index = {valor: [] for valor in voc_claus}
        # elif tipus_doc=="img":
            
    def afegeix_identificador(self,llista_paraules,pos_element,tipus_doc):
        if tipus_doc=="txt":
            for i in llista_paraules:
                if i in self._dict_index:
                    if pos_element not in self._dict_index[i]:
                        self._dict_index[i].append(pos_element)
        # elif tipus_doc=="img":
            
    def recupera_llista(self,obj,tipus):
        if tipus=="txt":
            paraules=obj.text
            llista_fitxers=[]

            for p in paraules:
                if p in self._dict_index:
                    llista_fitxers.extend(self._dict_index[p])
            result = []
            for item in llista_fitxers:
                if item not in result:
                    result.append(item)
            return result